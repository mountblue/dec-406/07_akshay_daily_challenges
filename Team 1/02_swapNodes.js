function LinkedList() {
    var length = 0;
    var head = null;

    var Node = function (value) {
        this.value = value;
        this.next = null;
    };

    this.size = function () {
        return length;
    };

    this.head = function () {
        return head;
    };

    this.add = function (value) {
        var node = new Node(value);
        if (head === null) {
            head = node;
        } else {
            var currentNode = head;
            while (currentNode.next) {
                currentNode = currentNode.next;
            }
            currentNode.next = node;
        }
        length++;
    };
    this.display = function () {
        var currentNode = head;
        while (currentNode) {
            console.log(currentNode.value);
            currentNode = currentNode.next;
        }
    }
    this.swap = function (n1, n2) {

        var prev1 = null;
        var prev2 = null;
        var temp1, temp2;
        if (n1 == n2) {
            return;
        }
        temp1 = head;
        while (temp1 != null && temp1.value != n1) {
            prev1 = temp1;
            temp1 = temp1.next;
        }
        temp2 = head;
        while (temp2 != null && temp2.value != n2) {
            prev2 = temp2;
            temp2 = temp2.next;
        }
        if (temp1 === null || temp2 === null)
            return;
        if (temp1 != null) {
            prev1.next = temp2;
        } else {
            head = temp2;
        }
        if (temp2 != null) {
            prev2.next = temp1;
        } else {
            head = temp1;
        }
        var temp = temp1.next;
        temp1.next = temp2.next;
        temp2.next = temp;
    };
}

function main(arr,n1,n2) {
    var list = new LinkedList();

    for(var i = 0; i < arr.length; i++) {
        list.add(arr[i])
    }
    // list.display();
    list.swap(n1,n2)
    list.display();
}
main([12,34,56,3,6,83,2],34,6)