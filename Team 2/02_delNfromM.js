function LinkedList() {
    var length = 0;
    var head = null;

    var Node = function (value) {
        this.value = value;
        this.next = null;
    };

    this.size = function () {
        return length;
    };

    this.head = function () {
        return head;
    };

    this.add = function (value) {
        var node = new Node(value);
        if (head === null) {
            head = node;
        } else {
            var currentNode = head;
            while (currentNode.next) {
                currentNode = currentNode.next;
            }
            currentNode.next = node;
        }
        length++;
    };
    this.display = function () {
        var currentNode = head;
        while (currentNode) {
            console.log(currentNode.value);
            currentNode = currentNode.next;
        }
    }
    this.SkipAndDel = function (m, n) {
        var currentNode = head;
        var count;

        while (currentNode) {
            // Skip M nodes
            for (count = 1; count < m && currentNode !== null; count++) {
                currentNode = currentNode.next;
            }
            if (currentNode === null) {
                return;
            }
            // Remove N nodes
            var temp = currentNode.next;
            var previousNode = currentNode;
            for (count = 1; count <= n && temp !== null; count++) {
                var t = temp;
                temp = temp.next;
                previousNode.next = currentNode.next;
            }
            currentNode.next = temp;
            currentNode = temp;
        }
    }
}

function main(arr,m,n) {
    var list = new LinkedList();
    for(var i = 0; i < arr.length; i++){
        list.add(arr[i])
    }
    list.SkipAndDel(m,n);
    list.display();
}
main([1,3,5,2,9,4],3,2);