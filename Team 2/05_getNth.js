function LinkedList() {
    var length = 0;
    var head = null;

    var Node = function (value) {
        this.value = value;
        this.next = null;
    };

    this.size = function () {
        return length;
    };

    this.head = function () {
        return head;
    };

    this.add = function (value) {
        var node = new Node(value);
        if (head === null) {
            head = node;
        } else {
            var currentNode = head;
            while (currentNode.next) {
                currentNode = currentNode.next;
            }
            currentNode.next = node;
        }
        length++;
    };
    this.display = function () {
        var currentNode = head;
        while (currentNode) {
            console.log(currentNode.value);
            currentNode = currentNode.next;
        }
    }
    this.GetNth = function (n){
        var currentNode = head;
        var index = 0;

        while(currentNode){
            if(index === n){
                console.log(currentNode.value)
                return currentNode.value;
            }
            index++;
            currentNode = currentNode.next;
        }
        return 0;
    }
}

function main(arr,n) {
    var list = new LinkedList();
    
    for(var i = 0; i < arr.length; i++){
        list.add(arr[i])
    }
    list.GetNth(n);
}
main([1,4,2,5,3,1,6,7,8],2);