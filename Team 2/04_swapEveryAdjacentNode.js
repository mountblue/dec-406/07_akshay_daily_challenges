function LinkedList() {
    var length = 0;
    var head = null;

    var Node = function (value) {
        this.value = value;
        this.next = null;
    };

    this.size = function () {
        return length;
    };

    this.head = function () {
        return head;
    };

    this.add = function (value) {
        var node = new Node(value);
        if (head === null) {
            head = node;
        } else {
            var currentNode = head;
            while (currentNode.next) {
                currentNode = currentNode.next;
            }
            currentNode.next = node;
        }
        length++;
    };
    this.display = function () {
        var currentNode = head;
        while (currentNode) {
            console.log(currentNode.value);
            currentNode = currentNode.next;
        }
    }
    this.swapTwo = function (){
        var currentNode = head;

        if(currentNode.next === null){
            return;
        }
        while(currentNode !== null && currentNode.next !== null){
            var temp = currentNode.value;
            currentNode.value = currentNode.next.value;
            currentNode.next.value = temp;
            currentNode = currentNode.next.next;
        }
    }
}

function main(arr) {
    var list = new LinkedList();
    
    for(var i = 0; i < arr.length; i++){
        list.add(arr[i])
    }
    list.swapTwo();
    list.display();
}
main([1,4,2,5,3,1,6,7,8]);