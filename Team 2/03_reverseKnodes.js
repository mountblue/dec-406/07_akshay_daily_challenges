function LinkedList() {
    var length = 0;
    var head = null;

    var Node = function (value) {
        this.value = value;
        this.next = null;
    };

    this.size = function () {
        return length;
    };

    this.head = function () {
        return head;
    };

    this.add = function (value) {
        var node = new Node(value);
        if (head === null) {
            head = node;
        } else {
            var currentNode = head;
            while (currentNode.next) {
                currentNode = currentNode.next;
            }
            currentNode.next = node;
        }
        length++;
    };
    this.display = function () {
        var currentNode = head;
        while (currentNode) {
            console.log(currentNode.value);
            currentNode = currentNode.next;
        }
    }
    this.reverseKNodes = function(node,k){
        var currentNode = node;
        var previousNode = null;
        var next = this.next;
        var count = 0;

        while(currentNode !== null && count < k){
            next = currentNode.next;
            currentNode.next = previousNode;
            previousNode = currentNode;
            currentNode = next;
            count++;
        }
        if(next !== null) {
            node.next = this.reverseKNodes(next,k);
        }
        
        return previousNode
    }
}

function main(arr,k) {
    var list = new LinkedList();
    
    for(var i = 0; i < arr.length; i++){
        list.add(arr[i])
    }
    list.reverseKNodes(list.head(),k);
    list.display();
}
main([1,4,2,5,3,1,6,7,8],2);