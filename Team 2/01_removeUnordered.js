function LinkedList() {
    var length = 0;
    var head = null;

    var Node = function (value) {
        this.value = value;
        this.next = null;
    };

    this.size = function () {
        return length;
    };

    this.head = function () {
        return head;
    };

    this.add = function (value) {
        var node = new Node(value);
        if (head === null) {
            head = node;
        } else {
            var currentNode = head;
            while (currentNode.next) {
                currentNode = currentNode.next;
            }
            currentNode.next = node;
        }
        length++;
    };
    this.display = function () {
        var currentNode = head;
        while (currentNode) {
            console.log(currentNode.value);
            currentNode = currentNode.next;
        }
    }
    this.unorderedRem = function(){
        var previousNode = head;
        var currentNode = head.next;
        if(previousNode === null){
            return;
        }
        while(currentNode) {
            if(currentNode.value >= previousNode.value){
                previousNode = currentNode;
                currentNode = currentNode.next;
            }else {
                previousNode.next = currentNode.next
                currentNode = currentNode.next;
            }
        }
    }
}
function main(arr) {
    var list = new LinkedList();
    
    for(var i = 0; i < arr.length; i++){
        list.add(arr[i])
    }
    list.unorderedRem();
    list.display();
    
}
main([1,4,2,3,6,6,10,4]);