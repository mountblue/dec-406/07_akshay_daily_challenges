function binaryTree() {
    var root = null;
    
    var Node = function(value) {
        this.value = value;
        this.right = null;
        this.left = null;
        this.show = show;
    }
    function show(){
        return this.value;
    }
    this.root =new Node(1);
    this.root.left = new Node(2);
    this.root.right = new Node(3);
    this.root.left.left = new Node(4);
    this.root.left.right = new Node(5);
    this.root.right.left = new Node(6);
    this.root.right.right = new Node(7);
    this.root.left.left.left = new Node(8);

    this.traverseNode = function(node,arr,node1){
        if(node === null){
            return;
        }
        arr.push(node.value);
        if(node.value === node1){
            return true;
        }
        if(node.left !== null && this.traverseNode(node.left,arr,node1) || (node.right !== null && this.traverseNode(node.right,arr,node1))){
            return true;
        }
        arr.pop();
        return false;
    }
    this.LowestAncestor = function(node,node1,node2){
        var arr1=[];
        var arr2=[];
        if(!(this.traverseNode(node,arr1,node1)) || !(this.traverseNode(node,arr2,node2))){
            return -1;
        }
        var c = 0;
        while( c < arr1.length && c < arr2.length){
            if(arr1[c]!==arr2[c]){
                break;
            }
            c++;
        }
        return (arr1[c-1]);
    }
}
function main(){
    var myTree = new binaryTree();
    var result = myTree.LowestAncestor(myTree.root,4,5);
    console.log(result);
 }
main();